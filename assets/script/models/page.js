/**
 * Created by r.rozhkov on 01.12.2016.
 */
var Page = (function () {
    function Page(_a) {
        var _b = _a.url, url = _b === void 0 ? '404' : _b, _c = _a.title, title = _c === void 0 ? '404 Not Found' : _c, _d = _a.desc, desc = _d === void 0 ? '' : _d, _e = _a.keys, keys = _e === void 0 ? '' : _e, _f = _a.ico, ico = _f === void 0 ? 'fa fa-chain-broken' : _f, _g = _a.html, html = _g === void 0 ? '404 Error to page.<br>Return to the main page' : _g, _h = _a.script, script = _h === void 0 ? '' : _h, _j = _a.style, style = _j === void 0 ? '' : _j;
        this.url = url.replace(/![a-zA-Z0-9-_]/gim, '');
        this.title = title.replace(/![\W\D\s]/gim, '');
        this.desc = desc.replace(/![\W\D\s]/gim, '');
        ;
        this.keys = keys.replace(/![\W\D\s]/gim, '');
        ;
        this.ico = ico.replace(/![\W\D\s-]/gim, '');
        ;
        this.html = html;
        this.script = script;
        this.style = style;
    }
    Page.prototype.getContent = function () {
        return "\n            <h1>" + (!this.ico ? "" : "<i class=\"" + this.ico + "\"></i>") + " " + this.title + "</h1>\n            <div>" + this.html + "</div>\n            " + (!this.style ? "" : "<style> " + this.style + " </style>") + "\n            " + (!this.script ? "" : "<script> " + this.script + " </script>") + "\n        ";
    };
    return Page;
}());
//# sourceMappingURL=page.js.map