/**
 * Created by r.rozhkov on 01.12.2016.
 */
/// <reference path="libs/es6.ts"/>
/// <reference path="libs/$_x.ts"/>
/// <reference path="models/page.ts"/>
var base_url = '';
$_x.load.add([
    (base_url + "assets/css/main.css"),
    (base_url + "favicon.ico"),
    (base_url + "assets/css/font-awesome.min.css"),
    (base_url + "assets/script/libs/history.min.js"),
    (base_url + "assets/script/models/page.js")
], ready);
var histAPI = !!(window.history && history.pushState);
var content = document.getElementById('content');
var time = Date.now();
var pages;
var log = function (msg) { return console.log((Date.now() - time) + "ms : " + msg); };
function ajaxHref(a, callback) {
    if (callback === void 0) { callback = null; }
    a.onclick = callback === null ? function (e) {
        if (histAPI) {
            e.preventDefault();
            console.log();
            openPage(a.getAttribute('href'));
            history.pushState(pages[(pages.length - 1)], pages[(pages.length - 1)].url, a.getAttribute('href'));
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            document.getElementById('aside').className = 'sliderClose';
            return false;
        }
    } : callback();
    return a;
}
function openPage(href) {
    var search = href.match(/\?page=([\w\d]*)/);
    var page = (search === null || !search[1]) ? 'index' : search[1];
    //проверка загружалась ли страница
    if (pages) {
        for (var key in pages) {
            if (pages[key].url == page) {
                parserHtml(pages[key]);
                return true;
            }
        }
    }
    //отправка GET запроса для получения данных
    $_x.http.get(base_url + "assets/api/page/" + page + ".json", function (e) {
        var profile = { url: '404' };
        if (e.status == 200) {
            profile = JSON.parse(e.data);
            profile.url = page;
        }
        pages = [new Page(profile)];
        parserHtml(pages[(pages.length - 1)]);
        var hrefs = content.getElementsByTagName('a');
        for (var key in hrefs)
            ajaxHref(hrefs[key]);
        log("Страница готова");
    });
    return true;
}
function parserHtml(page) {
    log("Вывод страницы");
    //вывод на страницу информацию
    content.innerHTML = page.getContent();
    //обработка мета тегов
    var meta = document.getElementsByTagName("meta");
    for (var key in meta)
        if (meta[key].name == "description")
            meta[key].content = page.desc;
        else if (meta[key].name == "keywords")
            meta[key].content = page.keys;
    document.title = page.title;
    loadDisqus();
    log("Конец построения страницы");
}
function ready() {
    log("Запуск после подгрузки всех файлов");
    window.onpopstate = function (e) {
        if (history.location)
            openPage(history.location.search);
    };
    //проверяем страницу
    openPage(window.location.search);
    //подгрузка профиля
    $_x.http.get(base_url + "assets/api/tpl/profile.json", function (e) {
        if (e.status == 200) {
            var profile = JSON.parse(e.data);
            var header = document.getElementById('header');
            var links = '';
            for (var key in profile.contscts)
                links += profile.contscts[key] ? "<a href=\"" + profile.contscts[key].site + "\" target=\"_blank\">\n                                                    <i class=\"" + profile.contscts[key].ico + "\" aria-hidden=\"true\"></i>\n                                                </a>" : '';
            header.innerHTML = "\n                <div class=\"center\">\n                    <h3>\n                        <i class=\"" + profile.ico + "\" aria-hidden=\"true\"></i> " + profile.name + " \n                        <span>" + profile.developer + "</span>\n                    </h3>\n                    <a id=\"min-menu\"><i class=\"fa fa-bars\" aria-hidden=\"true\"></i></a>\n                </div>";
            document.getElementById('profile').innerHTML = "<div class=\"textCenter\">\n                    <div class=\"avatar\"><img src='" + profile.avatar + "' alt='" + profile.name + "'></div>\n                    <a href=\"mailto:" + profile.email + "\">" + profile.email + "</a><br>\n                    <a href=\"tel:" + profile.phone + "\">" + profile.phone + "</a>\n                    " + (links ? "<div class=\"table\">" + links + "</div>" : '') + "\n                </div>\n                " + profile.html;
        }
        //переменные
        var minMenu = document.getElementById('min-menu');
        var aside = document.getElementById('aside');
        //функция скрытия по классу
        function openMenu(aside, fix) {
            if (fix === void 0) { fix = null; }
            aside.className = fix === null ? (aside.className == 'sliderOpen' ? 'sliderClose' : 'sliderOpen') : ((fix && aside.className == 'sliderClose' || fix && aside.className == '') ? 'sliderOpen' : ((!fix && aside.className == 'sliderOpen') ? 'sliderClose' : aside.className));
        }
        //функция скрытия по клику
        if (aside && minMenu) {
            minMenu.onclick = function () {
                openMenu(aside);
                return false;
            };
        }
        //функция скрытия по жесту
        $_x.touch().callback({
            lr: function () {
                openMenu(aside, true);
            },
            rl: function () {
                openMenu(aside, false);
            }
        });
        log("Профиль готов");
    });
    //подгрузка меню
    $_x.http.get(base_url + "assets/api/tpl/menu.json", function (e) {
        if (e.status == 200) {
            var menus = JSON.parse(e.data);
            var navigation = document.getElementById('navigation');
            navigation.innerHTML = "<h3 class=\"side-h3\"><i class=\"fa fa-id-card-o\" aria-hidden=\"true\"></i> \u041D\u0430\u0432\u0438\u0433\u0430\u0446\u0438\u044F</h3>\n                <menu class=\"side-list\"></menu>";
            navigation = navigation.getElementsByTagName('menu')[0];
            for (var key in menus) {
                var li = $_x.create('li').set({
                    'innerHTML': "<i class=\"" + menus[key].ico + "\" aria-hidden=\"true\"></i> "
                });
                if (!menus[key].href) {
                    li.get().innerHTML = li.get().innerHTML + " " + menus[key].title;
                    navigation.appendChild(li.get());
                }
                else {
                    var a = $_x.create('a').set({
                        'innerText': "" + menus[key].title
                    }).attr({
                        'href': "" + menus[key].href,
                        'className': 'ajaxLink'
                    });
                    navigation.appendChild(li.get()).appendChild(ajaxHref(a.get()));
                }
            }
        }
        log("Меню готово");
    });
    //подгрузка цитат
    $_x.http.get(base_url + "assets/api/tpl/copy.json", function (e) {
        if (e.status == 200) {
            var contents_1 = JSON.parse(e.data);
            var copy_1 = document.getElementById('copy');
            function setCopy() {
                var content = contents_1[Math.floor(Math.random() * contents_1.length)];
                copy_1.innerHTML = content.text + " <i class=\"fa fa-copyright\" aria-hidden=\"true\"></i> " + content.name;
            }
            setCopy();
            setInterval(setCopy, 30000);
        }
        log("Цитаты готовы");
    });
}
log('Просто запуск');
//# sourceMappingURL=controller.js.map