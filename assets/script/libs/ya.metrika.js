(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter41618754 = new Ya.Metrika({
                id:41618754,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                trackHash:true
            });
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");


// disqus
function loadDisqus()   {
    (function() {
        var d = document, s = d.createElement('script');
        s.type = 'text/javascript'; 
        s.async = true;
        s.src = '//https-rus-lan-bitbucket-io.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
}