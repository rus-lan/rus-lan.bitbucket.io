/**
 * Created by r.rozhkov on 01.12.2016.
 */

module $_x{
    /* подгрузить файлы в head */
    export class load {
        private static arr: any = {};
        private static loadList: number[] = [];
        private static obj: any = {
            css: {
                element: 'link',
                parent: { rel: 'stylesheet', type: "text/css" },
                url: 'href'
            },
            js: {
                element: 'script',
                parent: { charset: "utf-8", async: null },
                url: 'src'
            },
            png: {
                element: 'link',
                parent: { rel: "icon", type: 'image/png' },
                url: 'href'
            },
            ico: {
                element: 'link',
                parent: { rel: "shortcut icon", type: 'image/x-icon' },
                url: 'href'
            }
        };

        private static getObj(
            url: string,
            type: string,
            callback: any,
            parent: any,
            endCallback: any,
            uid: number = null,
        ): any {
            //определяем обьект
            if( type && !this.obj[type] ){
                return {};
            }else{
                let t: any = {};
                if( type && !this.arr[type]) this.arr[type] = [];
                for (let key in this.obj){
                    if( url.trim().endsWith(`.${key}`) || url.includes(`.${key}?`) ){
                        t = key;
                        break;
                    }
                }
                if( typeof t === "object" ) return t;
                else type = t;
            }
            //получаем последовательность объекта
            uid = uid === null ? ( this.arr[type] ? this.arr[type].length : 0 ) : uid;

            if( uid ) this.arr[type][uid] = this.obj[type];
                else this.arr[type] = [this.obj[type]];
            if( type == 'ico' ) endCallback.obj--;
            //возвращаем готовый объект
            return this.arr[type][uid] = this.obj[type].url ?
                this.createObj(
                    url.trim(),
                    this.arr[type][uid],
                    callback && typeof callback === "function" ? callback : function(){},
                    parent && typeof parent === "object" ? parent : {},
                    endCallback
                ) : {} ;
        }

        private static createObj(
            url: string,
            obj: any,
            callback: any,
            parent: any,
            endCallback: any
        ): HTMLElement {
            let elem = document.createElement(obj.element);
            for (let key in obj.parent) elem[key] = obj.parent[key];
            elem[obj.url] = url;
            for (let key in parent) elem[key] = parent[key];
            elem.onreadystatechange = elem.onload = function() {
                elem.onreadystatechange = elem.onload = null;
                callback();
                if( endCallback !== null && endCallback.obj && endCallback.callback ){
                    endCallback.obj--;
                    if(!endCallback.obj) endCallback.callback();
                }
            };
            document.getElementsByTagName("head")[0].appendChild(elem);
            return elem;
        }

        private static condom( arr: any, callback: any = {} ): any {
            return arr.url ? this.getObj(
                arr.url,
                typeof arr.type === "string" ? arr.type : '',
                typeof arr.callback === "function" ? arr.callback : function(){},
                typeof arr.parent === "object" ? arr.parent : {},
                callback
            ) : this.getObj(
                arr[0],
                typeof arr[1] === "string" ? arr[1] : '',
                typeof arr[2] === "function" ? arr[2] : function(){},
                typeof arr[3] === "object" ? arr[3] : {},
                callback
            );
        }

        static files( type: string = '' ): any {
            let list: any = {};
            for (let key in this.obj){
                if( type == key ) return this.arr[key];
                else list[key] = this.arr[key];
            }
            return list;
        }

        static remove( type: string = '', uid: number = null ): boolean {
            if(!type || !uid) return null;
            for (let key in this.obj){
                if( type == key && this.arr[key][uid] ){
                    this.arr[key][uid] = null;
                    return true;
                }
            }
            return false;
        }

        static get( type: string = '', uid: number = null ): any {
            if(!type || !uid) return null;
            for (let key in this.obj){
                if( type == key && this.arr[key][uid] ){
                    return this.arr[key][uid];
                }
            }
            return false;
        }

        static add( ...files: any[] ): load {
            if( typeof files === "object" && files[0] ){
                let callback = files[1] && typeof files[1] === "function" ? {
                    obj: this.loadList[files[0]] = files[0].length,
                    callback: files[1]
                } : null;

                if( typeof files[0] === "string" ) this.condom(files);
                else if( files[0].url ) this.condom(files[0]);
                else if( typeof files[0][0] === "string" ){
                    if( files[0][1] && !this[files[0][1]] && files[0][1].length > 3 )
                        for (let key in files[0] ) this.condom([files[0][key]], callback );
                    else if(!files[0][1] && files[1]) this.condom([files[0][0]], callback);
                    else this.condom(files[0]);
                }else if( files[0][0] && ( files[0][0].url || typeof files[0][0][0] === "string" ) )
                    for (let key in files[0] ) this.condom(files[0][key], callback );
            }
            return this;
        }
    }

    /* создать новый объект */
    class createElement {
        element: HTMLElement;
        constructor( tagName = 'div' ) {
            tagName = tagName.replace(/![a-zA-Z0-9-_]/gim, '');
            this.element = document.createElement(tagName ? tagName : 'div');
        }
        set( ...attr ): createElement {
            if ( attr[0] != undefined && attr[0]){
                if ( attr.length > 1 && attr[0] )
                    this.element[attr[0]] = attr[1];
                else
                    for (let key in attr[0]) this.element[key] = attr[0][key];
            }
            return this;
        }
        attr( ...attr ): createElement {
            if ( attr[0] != undefined && attr[0]){
                if ( attr.length > 1 && attr[0] )
                    this.element.setAttribute(attr[0], attr[1]);
                else
                    for (let key in attr[0]) this.element.setAttribute(key, attr[0][key]);
            }
            return this;
        }
        get( parent = '' ): HTMLElement {
            if( parent && this.element[parent] != undefined )
                return this.element[parent];
            return this.element;
        }
        getAttr( parent = '' ): string {
            if( parent && this.element.getAttribute(parent) != undefined )
                return this.element.getAttribute(parent);
            return "";
        }
    }
    /* функция для создания обьекта */
    export function create( str:string = '' ){
        return new createElement(str);
    }

    /* объект для отправки запросов */
    export class http {
        private static request(
            method:string,
            url:string,
            parent:any,
            callback: any
        ): void {
            let formData = new FormData();
            for (let key in parent)
                formData.append(key, parent[key]);

            let request = typeof XMLHttpRequest != 'undefined'
                ? new XMLHttpRequest()
                : new ActiveXObject('Microsoft.XMLHTTP');
            request.open( method, url, true );
            request.onreadystatechange = function() {
                if(request.readyState == 4){
                    callback( {
                        status: request.status,
                        data: request.responseText
                    } );
                }
            };
            request.send(formData);
        }

        static get( url: string = null, ...pars: any[] ): boolean {
            if(url === null) return false;
            let parent = {};
            let callback = function(){};
            for(let key in pars){
                if( typeof pars[key] === "function" ) callback = pars[key];
                else if( typeof pars[key] === "object" ) parent = pars[key];
            }
            this.request( 'GET', url, parent, callback );
            return true
        }

        static post( url: string = null, ...pars: any[] ): boolean {
            if(url === null) return false;
            let parent = {};
            let callback = function(){};
            for(let key in pars){
                if( typeof pars === "function" ) callback = pars[key];
                else if( typeof pars === "object" ) parent = pars[key];
            }
            this.request( 'POST', url, parent, callback );
            return true
        }
    }

    /* объект для отправки запросов */
    export class touchElement {
        private element: touchElement;
        private event = {
            start: {x:0, y:0},
            end: {x:0, y:0},
            sum: {x:0, y:0},
            module: {x:0, y:0}
        };
        private px = 100;
        private callbackList = {
            lr: function(){},
            rl: function(){},
            tb: function(){},
            bt: function(){}
        };
        constructor( element: Document ) {
            let te: touchElement = this;
            //стартовые точки
            element.addEventListener("touchstart", function(e){
                // e.preventDefault();
                te.event.start = {
                    x: e.changedTouches[0].clientX,
                    y: e.changedTouches[0].clientY
                };
            }, false);
            //конечные точки
            element.addEventListener("touchend", function(e){
                // e.preventDefault();
                te.event.end = {
                    x: e.changedTouches[0].clientX,
                    y: e.changedTouches[0].clientY
                };
                te.handleSum(te);
            }, false);
        }
        //расчеты
        private handleSum(te: touchElement): void {
            //разность точек
            te.event.sum = {
                x: te.event.start.x - te.event.end.x,
                y: te.event.start.y - te.event.end.y
            };
            //модуль разности
            te.event.module = {
                x: Math.abs(te.event.sum.x),
                y: Math.abs(te.event.sum.y)
            };
            //проверка точек
            if ( te.event.module.x > ( te.event.module.y*2 )
                && te.event.sum.x < 0
                && te.event.module.x > te.px ) te.callbackList.lr();
            else if ( te.event.module.x > ( te.event.module.y*2 )
                && te.event.sum.x > 0
                && te.event.module.x > te.px ) te.callbackList.rl();
            else if ( te.event.module.y > ( te.event.module.x*2 )
                && te.event.sum.y < 0
                && te.event.module.y > te.px ) te.callbackList.tb();
            else if ( te.event.module.y > ( te.event.module.x*2 )
                && te.event.sum.y > 0
                && te.event.module.y > te.px ) te.callbackList.bt();
        }
        //замена стандартных действия
        callback( obj = {} ): touchElement {
            for ( let key in obj )
                if( this.callbackList[key] && typeof obj[key] === "function" )
                    this.callbackList[key] = obj[key];
            return this;
        }
    }
    /* функция для создания обьекта */
    export function touch( obj: Document = document ){
        return new touchElement( obj );
    }
}

