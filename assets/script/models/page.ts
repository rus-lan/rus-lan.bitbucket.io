/**
 * Created by r.rozhkov on 01.12.2016.
 */

class Page {
    url: string;
    title: string;
    desc: string;
    keys: string;
    ico: string;
    html: string;
    script: string;
    style: string;

    constructor( {
        url = '404',
        title = '404 Not Found',
        desc = '',
        keys = '',
        ico = 'fa fa-chain-broken',
        html = '404 Error to page.<br>Return to the main page',
        script = '',
        style = ''
    } ) {
        this.url = url.replace(/![a-zA-Z0-9-_]/gim, '');
        this.title = title.replace(/![\W\D\s]/gim, '');
        this.desc = desc.replace(/![\W\D\s]/gim, '');;
        this.keys = keys.replace(/![\W\D\s]/gim, '');;
        this.ico = ico.replace(/![\W\D\s-]/gim, '');;
        this.html = html;
        this.script = script;
        this.style = style;
    }

    getContent(): string{
        return `
            <h1>${ !this.ico ? `` : `<i class="${this.ico}"></i>` } ${this.title}</h1>
            <div>${this.html}</div>
            ${ !this.style ? `` : `<style> ${this.style} </style>` }
            ${ !this.script ? `` : `<script> ${this.script} </script>` }
        `;
    }
}