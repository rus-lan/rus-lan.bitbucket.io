/**
 * Created by r.rozhkov on 01.12.2016.
 */
/// <reference path="libs/es6.ts"/>
/// <reference path="libs/$_x.ts"/>
/// <reference path="models/page.ts"/>

let base_url = '';

$_x.load.add([
    `${base_url}assets/css/main.css`,
    `${base_url}favicon.ico`,
    `${base_url}assets/css/font-awesome.min.css`,
    `${base_url}assets/script/libs/history.min.js`,
    `${base_url}assets/script/models/page.js`
], ready);

let histAPI = !!(window.history && history.pushState);
let content = document.getElementById('content')
let time = Date.now();
let pages: Page[];

let log = (msg: string): void => console.log(`${Date.now() - time}ms : ${msg}`);

function ajaxHref(a: HTMLElement, callback = null): HTMLElement {
    a.onclick = callback === null ? function(e): boolean{
        if (histAPI) {
            e.preventDefault();
            console.log();
            openPage(a.getAttribute('href'));
            history.pushState(pages[ (pages.length-1) ], pages[ (pages.length-1) ].url, a.getAttribute('href'));
            document.body.scrollTop = document.documentElement.scrollTop = 0;
            document.getElementById('aside').className = 'sliderClose';
            return false;
        }
    } : callback();
    return a;
}

function openPage(href: string): boolean {
    let search: string[] = href.match(/\?page=([\w\d]*)/);
    let page = ( search === null || !search[1] )? 'index' : search[1];
    //проверка загружалась ли страница
    if( pages ){
        for (let key in pages){
            if(pages[key].url == page){
                parserHtml( pages[key] );
                return true;
            }
        }
    }
    //отправка GET запроса для получения данных
    $_x.http.get(`${base_url}assets/api/page/${page}.json`, function(e): void{
        let profile = { url: '404' };
        if(e.status == 200){
            profile = JSON.parse(e.data);
            profile.url = page;
        }
        pages = [ new Page(profile) ];
        parserHtml( pages[(pages.length-1)] );

        let hrefs = content.getElementsByTagName('a');
        for (let key in hrefs) ajaxHref(hrefs[key]);

        log("Страница готова");
    });
    return true;
}

function parserHtml(page: Page ): void {
    log("Вывод страницы");
    //вывод на страницу информацию
    content.innerHTML = page.getContent();
    //обработка мета тегов
    let meta = document.getElementsByTagName("meta");
    for (let key in meta)
        if (meta[key].name == "description")
            meta[key].content = page.desc;
        else if(meta[key].name == "keywords")
            meta[key].content = page.keys;
    document.title = page.title;
    loadDisqus();
    log("Конец построения страницы");
}

function ready() {
    log("Запуск после подгрузки всех файлов");

    window.onpopstate = function( e ) {
        if( history.location )
            openPage(history.location.search);
    };

    //проверяем страницу
    openPage(window.location.search);

    //подгрузка профиля
    $_x.http.get(`${base_url}assets/api/tpl/profile.json`, function(e): void{
        if(e.status == 200){
            let profile = JSON.parse(e.data);
            let header = document.getElementById('header');
            let links = '';
            for (let key in profile.contscts)
                links += profile.contscts[key] ? `<a href="${profile.contscts[key].site}" target="_blank">
                                                    <i class="${profile.contscts[key].ico}" aria-hidden="true"></i>
                                                </a>` : '';
            header.innerHTML = `
                <div class="center">
                    <h3>
                        <i class="${profile.ico}" aria-hidden="true"></i> ${profile.name} 
                        <span>${profile.developer}</span>
                    </h3>
                    <a id="min-menu"><i class="fa fa-bars" aria-hidden="true"></i></a>
                </div>`;
            document.getElementById('profile').innerHTML = `<div class="textCenter">
                    <div class="avatar"><img src='${profile.avatar}' alt='${profile.name}'></div>
                    <a href="mailto:${profile.email}">${profile.email}</a><br>
                    <a href="tel:${profile.phone}">${profile.phone}</a>
                    ${ links ? `<div class="table">${links}</div>` : '' }
                </div>
                ${profile.html}`;
        }
        //переменные
        let minMenu = document.getElementById('min-menu');
        let aside = document.getElementById('aside');
        //функция скрытия по классу
        function openMenu( aside: HTMLElement, fix: boolean = null ): void{
            aside.className = fix === null ? (
                aside.className == 'sliderOpen' ? 'sliderClose' : 'sliderOpen'
            ) : (
                ( fix && aside.className == 'sliderClose' || fix && aside.className == '' ) ? 'sliderOpen' : (
                    ( !fix && aside.className == 'sliderOpen' ) ? 'sliderClose' : aside.className
                )
            );
        }
        //функция скрытия по клику
        if(aside && minMenu){
            minMenu.onclick = function():boolean {
                openMenu( aside )
                return false;
            };
        }
        //функция скрытия по жесту
        $_x.touch().callback({
            lr: function(){
                openMenu( aside, true )
            },
            rl: function(){
                openMenu( aside, false )
            }
        });
        log("Профиль готов");
    });
    //подгрузка меню
    $_x.http.get(`${base_url}assets/api/tpl/menu.json`, function(e): void{
        if(e.status == 200){
            let menus = JSON.parse(e.data);
            let navigation = document.getElementById('navigation');
            navigation.innerHTML = `<h3 class="side-h3"><i class="fa fa-id-card-o" aria-hidden="true"></i> Навигация</h3>
                <menu class="side-list"></menu>`;
            navigation = navigation.getElementsByTagName('menu')[0];
            for (let key in menus ){
                let li = $_x.create('li').set({
                    'innerHTML': `<i class="${menus[key].ico}" aria-hidden="true"></i> `
                });
                if( !menus[key].href ){ //если не ссылка
                    li.get().innerHTML = `${li.get().innerHTML} ${menus[key].title}`;
                    navigation.appendChild(
                        li.get()
                    );
                }else{ // если ссылка
                    let a = $_x.create('a').set({
                        'innerText': `${menus[key].title}`
                    }).attr({
                        'href': `${menus[key].href}`,
                        'className': 'ajaxLink'
                    });
                    navigation.appendChild(
                        li.get()
                    ).appendChild(
                        ajaxHref( a.get() )
                    );
                }
            }
        }
        log("Меню готово");
    });
    //подгрузка цитат
    $_x.http.get(`${base_url}assets/api/tpl/copy.json`, function(e): void{
        if(e.status == 200){
            let contents = JSON.parse(e.data);
            let copy = document.getElementById('copy');
            function setCopy(): void{
                let content = contents[Math.floor(Math.random() * contents.length)];
                copy.innerHTML = `${content.text} <i class="fa fa-copyright" aria-hidden="true"></i> ${content.name}`;
            }
            setCopy();
            setInterval(setCopy, 30000);
        }
        log("Цитаты готовы");
    });

}
log('Просто запуск');

