/**
 * Created by r.rozhkov on 01.12.2016.
 */

if(!String.prototype.endsWith){
    String.prototype.endsWith = function(search: string, position: number): boolean {
        let subjectString = this.toString();
        if (typeof position !== 'number' ||
            !isFinite(position) ||
            Math.floor(position) !== position ||
            position > subjectString.length
        ) position = subjectString.length;
        position -= search.length;
        let lastIndex = subjectString.lastIndexOf(search, position);
        return lastIndex !== -1 && lastIndex === position;
    };
}

if(!String.prototype.startsWith){
    String.prototype.startsWith = function(search: string, position: number): boolean {
        position = position || 0;
        return this.substr(position, search.length) === search;
    };
}

if(!String.prototype.includes){
    String.prototype.includes = function(search: string, start: number): boolean {
        'use strict';
        if (typeof start !== 'number') start = 0;
        return (start + search.length > this.length) ? false : this.indexOf(search, start) !== -1;
    };
}
